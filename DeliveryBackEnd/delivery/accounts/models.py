from django.db import models
from django.utils.translation import gettext as _
from django.contrib.auth.models import AbstractUser

from rest_framework.authtoken.models import Token

from model_utils.models import SoftDeletableModel
from core.models import TimeStampedModel


class User(AbstractUser, SoftDeletableModel, TimeStampedModel):

    environment = models.CharField(
        max_length=75,
        choices=(
            ('SANDBOX', 'Sandbox'),
            ('PRODUCTION', 'Producción'),
        ),
        default='SANDBOX'
    )
    phone = models.CharField(max_length=12)
    image = models.ImageField(upload_to='users/', default='no-image.jpg', blank=True)
    role = models.CharField(max_length=50)
    job_position = models.CharField(max_length=50)

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __str__(self):
        return str(self.username)

    @property
    def access_token(self):
        token, created = Token.objects.get_or_create(user=self)
        return token.key
