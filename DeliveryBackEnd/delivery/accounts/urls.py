from django.urls import path
from . import views as user_views


urlpatterns = [

    path('users/', user_views.UserList.as_view(), name='user-list'),
    path('user/<int:id>/', user_views.UserDetail.as_view(), name='user-detail'),
    path('users/create/', user_views.UserCreate.as_view(), name='user-create'),
    path('users/edit/<int:pk>/', user_views.UserUpdate.as_view(), name='user-update'),
    path('users/delete/<int:pk>/', user_views.UserDelete.as_view(), name='user-delete'),

]
