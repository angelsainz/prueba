import json

from core.api_mixins import DefaultMixins

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import User
from .serializers import UserSerializer


class UserList(DefaultMixins, APIView):

    def get(self, request, *args, **kwargs):
        queryset = User.objects.all()
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserDetail(DefaultMixins, APIView):

    def get(self, request, *args, **kwargs):

        user = User.objects.filter(pk=kwargs.get('id')).first()
        if not user:
            return Response({"detail": "User not found"}, status=status.HTTP_404_NOT_FOUND)

        serializer = UserSerializer(user)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserCreate(APIView):

    def post(self, request, *args, **kwargs):

        if request.__class__.__name__ != 'WSGIRequest':
            data = request.data.copy()
        else:
            data = request.POST.copy()

        if not data:
            data = json.loads(request.body)

        serializer = UserSerializer(data=data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_406_NOT_ACCEPTABLE)

        serializer.save()
        return Response({"detail": "User created successfully"}, status=status.HTTP_201_CREATED)


class UserUpdate(DefaultMixins, APIView):

    def put(self, request, *args, **kwargs):

        user = User.objects.filter(pk=kwargs.get('pk')).first()
        if not user:
            return Response({"detail": "User not found"}, status=status.HTTP_404_NOT_FOUND)

        if request.__class__.__name__ != 'WSGIRequest':
            data = request.data.copy()
        else:
            data = request.POST.copy()

        if not data:
            data = json.loads(request.body)

        serializer = UserSerializer(user, data=data, partial=True)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_406_NOT_ACCEPTABLE)

        serializer.save()
        return Response({"detail": "User edited successfully"}, status=status.HTTP_200_OK)


class UserDelete(DefaultMixins, APIView):

    def delete(self, request, *args, **kwargs):

        user = User.objects.filter(pk=kwargs.get('pk')).first()
        if not user:
            return Response({"detail": "User not found"}, status=status.HTTP_404_NOT_FOUND)

        user.delete()
        return Response({"detail": "User deleted successfully"}, status=status.HTTP_200_OK)
